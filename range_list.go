package rangelist

import (
	"fmt"
	"sync"
)

// NewRangeList provides the ability to quickly initialize RangeList and
// complete the initialization of the associated variables;
// Note: that you must use NewRangeList() or NewRangeListWithElement()  to obtain RangeList
func NewRangeList() *RangeList {
	return &RangeList{
		lock: &sync.RWMutex{},
		list: make([][2]int, 0),
	}
}

// NewRangeListWithElement Similar to NewRangeList,
// additionally he will return RangeList with the initial elements
func NewRangeListWithElement(e [2]int) *RangeList {
	r := NewRangeList()
	_ = r.Add(e)
	return r
}

// RangeList is used to track the range of numbers, you can
// add, remove, query, and print the relevant numbers, it is concurrently safe.
// Note: the number range is a half-open interval, [left, right) denotes all the
// real numbers x where left <= x < right,for example [2,7) means 2 <= numbers < 7
type RangeList struct {
	lock *sync.RWMutex

	// list stores all number ranges in order.
	// using an array of length 2 for each element instead of a
	// slice to represent the interval ensures that the index does not go out of boundary, avoiding panic
	list [][2]int
}

func (r *RangeList) Add(e [2]int) (err error) {
	defer func() {
		if p := recover(); p != nil {
			err = fmt.Errorf("you cannot directly declare and use RangeList")
		}
	}()
	r.lock.Lock()
	defer r.lock.Unlock()
	left, right := r.boundary(e)
	if left <= right {
		e[0] = min(e[0], r.list[left][0])  // use smaller number as left boundary
		e[1] = max(e[1], r.list[right][1]) // use larger number as right boundary
	}
	r.replaceRangeWithElements(left, right, [][2]int{{e[0], e[1]}}) // replace all elements in the range
	return nil
}

func (r *RangeList) Remove(e [2]int) (err error) {
	defer func() {
		if p := recover(); p != nil {
			err = fmt.Errorf("you cannot directly declare and use RangeList")
		}
	}()
	r.lock.Lock()
	defer r.lock.Unlock()
	left, right := r.boundary(e)
	// When an element is removed, the interval may be split into 0-2 elements
	split := make([][2]int, 0)
	for m := left; m <= right; m++ {
		// The remaining number on the left boundary becomes a new element
		if r.list[m][0] < e[0] {
			split = append(split, [2]int{r.list[m][0], e[0]})
		}
		// The remaining number on the right boundary becomes a new element
		if r.list[m][1] > e[1] {
			split = append(split, [2]int{e[1], r.list[m][1]})
		}
	}
	r.replaceRangeWithElements(left, right, split) // replace all elements in the range
	return nil
}

// Query use the binary search algorithm to find the interval where the element is located and return the judgment result.
// If the interval does not exist, return false.
func (r *RangeList) Query(e [2]int) bool {
	if len(r.list) == 0 {
		return false
	}
	r.lock.RLock()
	defer r.lock.RUnlock()
	i, j := 0, len(r.list)-1
	for i <= j {
		m := i + (j-i)/2
		if r.list[m][0] >= e[1] {
			j = m - 1
		} else if r.list[m][1] <= e[0] {
			i = m + 1
		} else {
			return r.list[m][0] <= e[0] && r.list[m][1] >= e[1]
		}
	}
	return false
}

func (r *RangeList) Print() error {
	r.lock.RLock()
	defer r.lock.RUnlock()
	for k, v := range r.list {
		// is not last element
		if k+1 < len(r.list) {
			fmt.Printf("[%d, %d) ", v[0], v[1])
		} else {
			fmt.Printf("[%d, %d)\n", v[0], v[1])
		}
	}
	return nil
}

// determine the left and right boundary based on element
func (r *RangeList) boundary(e [2]int) (left, right int) {
	left, right = 0, len(r.list)-1
	// find the first position that greater than or equal to e[0]
	for left < len(r.list) && r.list[left][1] < e[0] {
		left++
	}
	// find the first position that less than or equal to e[1]
	for right >= 0 && r.list[right][0] > e[1] {
		right--
	}
	return left, right
}

// Remove the element between left and right. then replace it with elements
func (r *RangeList) replaceRangeWithElements(left, right int, elements [][2]int) {
	r.list = append(r.list[:left], append(elements, r.list[right+1:]...)...)
}

func min(n1, n2 int) int {
	if n1 < n2 {
		return n1
	}
	return n2
}

func max(n1, n2 int) int {
	if n1 > n2 {
		return n1
	}
	return n2
}
