package rangelist

import (
	"reflect"
	"testing"
)

func TestRangeList_Add(t *testing.T) {
	rl := NewRangeList()
	err := rl.Add([2]int{10, 20})
	if err != nil {
		t.Errorf("add range failed, err: %v", err)
	}
	_ = rl.Add([2]int{15, 30})
	deepEqual(t, "Remove (1)", [][2]int{{10, 30}}, rl.list)
}

func TestRangeList_Remove(t *testing.T) {
	rl := NewRangeList()
	err := rl.Add([2]int{10, 20})
	if err != nil {
		t.Errorf("add range failed, err: %v", err)
	}
	err = rl.Remove([2]int{15, 30})
	if err != nil {
		t.Errorf("remove range failed, err: %v", err)
	}
	deepEqual(t, "Remove (1)", [][2]int{{10, 15}}, rl.list)
}

func TestRangeList_Query(t *testing.T) {
	rl := NewRangeList()
	err := rl.Add([2]int{10, 20})
	if err != nil {
		t.Errorf("add range failed, err: %v", err)
	}

	if !rl.Query([2]int{10, 20}) {
		t.Errorf("query range error, except: %v, got: %v", true, false)
	}
	if rl.Query([2]int{10, 21}) {
		t.Errorf("query range error, except: %v, got: %v", false, true)
	}
}

func deepEqual(t *testing.T, name string, except, got [][2]int) {
	if !reflect.DeepEqual(except, got) {
		t.Errorf("%s: Should display: %v, Got: %v", name, except, got)
	}
}

func TestRangeList_Print(t *testing.T) {
	rl := NewRangeList()
	rl.Add([2]int{1, 5})
	rl.Print()
	deepEqual(t, "Print (1)", [][2]int{{1, 5}}, rl.list)
	// Should display: [1, 5)

	rl.Add([2]int{10, 20})
	rl.Print()
	deepEqual(t, "Print (2)", [][2]int{{1, 5}, {10, 20}}, rl.list)
	// Should display: [1, 5) [10, 20)

	rl.Add([2]int{20, 20})
	rl.Print()
	deepEqual(t, "Print (3)", [][2]int{{1, 5}, {10, 20}}, rl.list)
	// Should display: [1, 5) [10, 20)

	rl.Add([2]int{20, 21})
	rl.Print()
	deepEqual(t, "Print (4)", [][2]int{{1, 5}, {10, 21}}, rl.list)
	// Should display: [1, 5) [10, 21)

	rl.Add([2]int{2, 4})
	rl.Print()
	deepEqual(t, "Print (5)", [][2]int{{1, 5}, {10, 21}}, rl.list)
	// Should display: [1, 5) [10, 21)

	rl.Add([2]int{3, 8})
	rl.Print()
	deepEqual(t, "Print (6)", [][2]int{{1, 8}, {10, 21}}, rl.list)
	// Should display: [1, 8) [10, 21)

	rl.Remove([2]int{10, 10})
	rl.Print()
	deepEqual(t, "Print (7)", [][2]int{{1, 8}, {10, 21}}, rl.list)
	// Should display: [1, 8) [10, 21)

	rl.Remove([2]int{10, 11})
	rl.Print()
	deepEqual(t, "Print (8)", [][2]int{{1, 8}, {11, 21}}, rl.list)
	// Should display: [1, 8) [11, 21)

	rl.Remove([2]int{15, 17})
	rl.Print()
	deepEqual(t, "Print (9)", [][2]int{{1, 8}, {11, 15}, {17, 21}}, rl.list)
	// Should display: [1, 8) [11, 15) [17, 21)

	rl.Remove([2]int{3, 19})
	rl.Print()
	deepEqual(t, "Print (10)", [][2]int{{1, 3}, {19, 21}}, rl.list)
	//Should display: [1, 3) [19, 21)
}

func TestRangeList_boundary(t *testing.T) {
	rl := NewRangeList()
	rl.Add([2]int{10, 20})
	rl.Add([2]int{30, 50})
	rl.Add([2]int{51, 100})
	l, r := rl.boundary([2]int{5, 15})
	if l != 0 || r != 0 {
		t.Errorf("find foundary err, except left-right: %d-%d, got left-right: %d-%d", 0, 0, l, r)
	}

	l, r = rl.boundary([2]int{11, 31})
	if l != 0 || r != 1 {
		t.Errorf("find foundary err, except left-right: %d-%d, got left-right: %d-%d", 0, 1, l, r)
	}

	l, r = rl.boundary([2]int{11, 51})
	if l != 0 || r != 2 {
		t.Errorf("find foundary err, except left-right: %d-%d, got left-right: %d-%d", 0, 2, l, r)
	}

	l, r = rl.boundary([2]int{101, 200})
	if l != 3 || r != 2 {
		t.Errorf("find foundary err, except left-right: %d-%d, got left-right: %d-%d", 3, 2, l, r)
	}
}
