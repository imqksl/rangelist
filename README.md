# RangeList

`RangeList` is used to track the range of numbers, you can
add, remove, query, and print the relevant numbers.

the number range is a half-open interval, [left, right) denotes all the
real numbers x where left <= x < right,for example [2,7) means 2 <= numbers < 7

# Features

- [x] Concurrently safe
- [x] Add Element
- [x] Remove Element
- [x] Query Element
- [x] Print Element

# Get Started

The following example shows you how to quickly use RangeList:

> you need to use NewRangeList() or NewRangeListWithElement() to obtain RangeList

```go
package main

import "gitlab.com/imqksl/rangelist"

func main() {
	rl:=rangelist.NewRangeList()
	rl.Add([2]int{1, 5})
	rl.Print()
	// Should display: [1, 5)

	rl.Add([2]int{10, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
}
```

## Remove&Query

The following example shows other methods of RangeList, including Remove and Query:

```go
package main

import "gitlab.com/imqksl/rangelist"

func main() {
	rl:=rangelist.NewRangeList()
	rl.Add([2]int{1, 5})
	rl.Print()
	// Should display: [1, 5)

	rl.Add([2]int{10, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)

	rl.Add([2]int{20, 21})
	rl.Print()
	// Should display: [1, 5) [10, 21)

	rl.Add([2]int{2, 4})
	rl.Print()
	// Should display: [1, 5) [10, 21)

	rl.Add([2]int{3, 8})
	rl.Print()
	// Should display: [1, 8) [10, 21)

	rl.Remove([2]int{10, 10})
	rl.Print()
	// Should display: [1, 8) [10, 21)

	rl.Remove([2]int{10, 11})
	rl.Print()
	// Should display: [1, 8) [11, 21)

	rl.Remove([2]int{15, 17})
	rl.Print()
	// Should display: [1, 8) [11, 15) [17, 21)

	rl.Remove([2]int{3, 19})
	rl.Print()
	//Should display: [1, 3) [19, 21)
}
```